﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace elevador_3
{
    class Program
    {
       
            public class A
        {
            public A(int a)
            {
                Console.WriteLine("Constructor de la clase A");
                Console.WriteLine(a);
            }
        }

        public class B : A
        {
            public B(int b) : base(b / 2)
            {
                Console.WriteLine("Constructor de la clase B");
                Console.WriteLine(b);
            }
        }

        public class C : B
        {
            public C(int c) : base(c / 2)
            {
                Console.WriteLine("Constructor de la clase C");
                Console.WriteLine(c);
            }
        }

        class Prueba
        {
            static void Main(string[] args)
            {
                C obj1 = new C(30);
                Console.ReadKey();
            }
        }
    }
}

