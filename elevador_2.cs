﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace constructor_2
{
    
      
    namespace Contacto
    {
        public class Contacto
        {
            public string Nombre;
            public string Apellido;
            public int Telefono;
            public string Direccion;
            public void SetContacto(String Nombre)
            {
                this.Nombre = Nombre;
            }
            public void SetContacto1(String Apellido)
            {
                this.Apellido = Apellido;
            }
            public void SetContacto2(int Telefono)
            {
                this.Telefono = Telefono;
            }
            public void SetContacto3(string Direccion)
            {
                this.Direccion = Direccion;
            }
            public void Saludar()
            {
                Console.WriteLine("Hola soy " + Nombre + " " + Apellido + ", y vivo en " + Direccion + ", mi telefono es " + Telefono + "");
            }
            public class ProbarContacto
            {
                public static void Main()
                {
                    Contacto cont1 = new Contacto();
                    Contacto cont2 = new Contacto();
                    cont1.SetContacto("pedro ");
                    cont1.SetContacto1("ramirez");
                    cont1.SetContacto2(829478652);
                    cont1.SetContacto3("canada");
                    cont2.SetContacto("rosa");
                    cont2.SetContacto1("alvarez");
                    cont2.SetContacto2(809132456);
                    cont2.SetContacto3("new york");
                    cont1.Saludar();
                    cont2.Saludar();
                    Console.ReadKey();
                }
            }   
     }  }
}
    

